﻿using UnityEngine;
using System.Collections;

public class Brick : MonoBehaviour {

    public AudioClip crack;
    public Sprite[] hitSprites;
    public static int breakableCount = 0;
    public GameObject smoke;

    private int timesHit;
    private LevelManager levelManager;
    private bool isBreakable;

	void Start () {
        isBreakable = (this.tag == "Breakable");
        if (isBreakable) {
            breakableCount++;
        }

        timesHit = 0;
        levelManager = GameObject.FindObjectOfType<LevelManager>();
	}
	
	void Update () {
	
	}

    void OnCollisionEnter2D(Collision2D col) {

        AudioSource.PlayClipAtPoint(crack, transform.position);
        if (isBreakable) {
            print("handle hits!");
            HandleHits();
        }
    }

    void HandleHits() {
        timesHit++;
        int maxHits = hitSprites.Length + 1;
        if (timesHit >= maxHits) {
            breakableCount--;
            levelManager.BrickDestroyed();
            PuffSmoke();
            Destroy(gameObject);
            print("Destroyed!");
        }
        else {
            LoadSprites();
        }
    }

    void PuffSmoke() {
        GameObject puff = (GameObject)Instantiate(smoke, gameObject.transform.position, Quaternion.identity);
        puff.GetComponent<ParticleSystem>().startColor = gameObject.GetComponent<SpriteRenderer>().color;
    }

    void LoadSprites() {
        int spriteIndex = timesHit - 1;
        if (hitSprites[spriteIndex]) {
            this.GetComponent<SpriteRenderer>().sprite = hitSprites[spriteIndex];
        }
    }

    // TODO remove this!
    void SimulateWin() {
        levelManager.LoadNextLevel();
    }
}
