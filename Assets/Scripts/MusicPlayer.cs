﻿using UnityEngine;
using System.Collections;

public class MusicPlayer : MonoBehaviour {

    private static MusicPlayer instance = null;

    void Awake() {
        print("Awake MusicPlayer");
    }

    void Start () {

        print("Start MusicPlayer");

        if (instance != null) {
            Destroy(gameObject);
            print("Duplicate music player self-destructed!");
        }
        else {
            instance = this;
            GameObject.DontDestroyOnLoad(gameObject);
        }
    }

    void Update () {
	
	}
}
