﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {

    private Paddle paddle;

    private bool hasStarted = false;
    private Vector3 paddleToBallVector;
    private Rigidbody2D rigi;
    private AudioSource mySound;

    private float BALLSPEED = 7f;

    private void Awake() {
        rigi = GetComponent<Rigidbody2D>();
        mySound = GetComponent<AudioSource>();
    }

	void Start () {
        paddle = GameObject.FindObjectOfType<Paddle>();
        paddleToBallVector = this.transform.position - paddle.transform.position;
    }
	
	void Update () {

        if (!hasStarted) {
            this.transform.position = paddle.transform.position + paddleToBallVector;

            if (Input.GetMouseButtonDown(0)) {
                hasStarted = true;
                print("Mouse clicked!");
                rigi.velocity = new Vector2(2f, BALLSPEED);
            }
        }
    }

    void OnCollisionEnter2D(Collision2D col) {

        Vector2 tweak = new Vector2(Random.Range(0f, 0.2f), Random.Range(0f, 0.2f));
        if (hasStarted) {
            mySound.Play();
            rigi.velocity += tweak;
        }
    }
}
